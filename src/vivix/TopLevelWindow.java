package vivix;

import javax.swing.*;
import java.awt.*;


// THIS IS DEPRECATED
// AN ATTEMPT AT MAKING A WINDOW

public class TopLevelWindow {
    public static void createWindow(){
        //Setting vivix.Main Frame
        JFrame mainFrame = new JFrame("Vivix Multi Attack Calculator");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Creating vivix.Main output Scrolling Display.
        JTextArea outDisplay = new JTextArea(0, SwingConstants.CENTER);
        outDisplay.setEditable(false);
        JScrollPane scrollOutDisplay = new JScrollPane(outDisplay);
        scrollOutDisplay.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollOutDisplay.setPreferredSize(new Dimension(500, 250));


        //Creating Output Sub window
        JPanel outputWindow = new JPanel();
        outputWindow.add(scrollOutDisplay, BorderLayout.NORTH);

        JButton clearButton = new JButton("Clear");
        //button.setPreferredSize(new Dimension(100, 100));

        //setting actions to buttons
        clearButton.addActionListener(e -> {
            outDisplay.setText("");
        });

        outputWindow.add(clearButton, BorderLayout.SOUTH);

        //Creating Subframe for MultiAttack
        JPanel multiAttackWindow = new JPanel();

        JLabel multiAttackLabel = new JLabel("MultiAttack:", SwingConstants.LEFT);
        //textLabel.setPreferredSize(new Dimension(300, 100));

        JButton clawMultiAttackButton = new JButton("Bite + Claw + Claw");
        //button.setPreferredSize(new Dimension(100, 100));

        JButton tailMultiAttackButton = new JButton("Bite + Claw + Tail");
        //button.setPreferredSize(new Dimension(100, 100));

        //setting actions to buttons
        clawMultiAttackButton.addActionListener(e -> {
            outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doMultiAttack(false));
        });

        tailMultiAttackButton.addActionListener(e -> {
            outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doMultiAttack(true));
        });

        //attaching buttons to sub window
        multiAttackWindow.add(multiAttackLabel, BorderLayout.NORTH);
        multiAttackWindow.add(clawMultiAttackButton, BorderLayout.CENTER);
        multiAttackWindow.add(tailMultiAttackButton, BorderLayout.SOUTH);


        //creating indivitual attack window
        JPanel indiAttackWindow = new JPanel();

        JLabel indiAttackLabel = new JLabel("Individual Attacks:", SwingConstants.LEFT);
        //textLabel.setPreferredSize(new Dimension(300, 100));

        JButton biteAttackButton = new JButton(("Bite"));

        JButton clawAttackButton = new JButton("Claw");

        JButton tailAttackButton = new JButton("Tail");

        //setting actions to buttons
        biteAttackButton.addActionListener(e -> outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doAttack(Data.Bite)));

        clawAttackButton.addActionListener(e -> outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doAttack(Data.Claw)));

        tailAttackButton.addActionListener(e -> outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doAttack(Data.Tail)));

        indiAttackWindow.add(indiAttackLabel, BorderLayout.EAST);
        indiAttackWindow.add(biteAttackButton, BorderLayout.NORTH);
        indiAttackWindow.add(clawAttackButton, BorderLayout.CENTER);
        indiAttackWindow.add(tailAttackButton, BorderLayout.SOUTH);

        //creating breath attack sub window
        JPanel breathAttackWindow = new JPanel();

        JLabel breathAttackLabel = new JLabel("Breath Attack:", SwingConstants.LEFT);
        //textLabel.setPreferredSize(new Dimension(300, 100));

        JButton lightningBeamButton = new JButton("Lightning Beam");
        //button.setPreferredSize(new Dimension(100, 100));

        lightningBeamButton.addActionListener(e -> {
            outDisplay.setText(outDisplay.getText() + "\n" + line() + Vivix.doBreathAttack());
        });

        //attaching breath stuff to sub window.
        breathAttackWindow.add(breathAttackLabel, BorderLayout.CENTER);
        breathAttackWindow.add(lightningBeamButton);

        //attaching windows to main frame
        mainFrame.getContentPane().add(multiAttackWindow, BorderLayout.WEST);
        mainFrame.getContentPane().add(breathAttackWindow, BorderLayout.EAST);
        mainFrame.getContentPane().add(indiAttackWindow, BorderLayout.NORTH);
        mainFrame.getContentPane().add(outputWindow, BorderLayout.SOUTH);

        mainFrame.setLocationRelativeTo(null);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private static String line() {
        return "- - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
    }
}
