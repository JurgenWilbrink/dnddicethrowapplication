package vivix;

public class DragonAttackType {

    public String type;
    public int dice, addHit, addDamage;

    public DragonAttackType(String type, int dice, int addHit, int addDamage) {
        this.type = type;
        this.dice = dice;
        this.addHit = addHit;
        this.addDamage = addDamage;
    }
}
