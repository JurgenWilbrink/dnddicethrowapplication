package vivix;

public class Data {
    // BASE Worths
    // Claw 6, 8, 5
    // Bite 10, 8, 5
    // Tail 8, 8, 7
    // Adding prof bonus "Add proficiency bonus to Your companions attack and damage rolls.
    // Current BONUS: 4 (DONE)
    public static DragonAttackType Claw = new DragonAttackType("Claw", 6, 12, 9);
    public static DragonAttackType Bite = new DragonAttackType("Bite", 10, 12, 9);
    public static DragonAttackType Tail = new DragonAttackType("Tail", 8, 12, 11);


}
