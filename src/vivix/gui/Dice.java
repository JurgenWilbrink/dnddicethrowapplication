package vivix.gui;

import java.util.Random;

public class Dice {

    private static Random rand = new Random();

    public static String rollMore(int amount, int size, int prof, int bonus) {

        String out = "Normal die roll: " + amount + "d" + size;

        if (amount > 1) {
            int total = 0;
            int[] rolls = new int[amount];

            for (int i = 0; i < amount; i++) {
                int x = roll(size);
                rolls[i] = x;
                total += x;
            }

            out = boneses(out, prof, bonus);

            out += "\nRoll: ";

            for (int i = 0; i < rolls.length; i++) {
                out += rolls[i];
                if (i == rolls.length - 1) {
                    out += " ";
                } else {
                    out += " + ";
                }
            }

            out += " = " + total;

            out = boneses(out, prof, bonus) + "\n";

            total += (prof + bonus);

            out += "Total Roll: " + total;

        } else {
            int x = roll(size);

            out = boneses(out, prof, bonus);

            out += "\nRoll: " + x;

            out = boneses(out, prof, bonus);

            x += (prof + bonus);

            out += "\nTotal Roll: " + x;
        }

        return out + "\n";
    }

    public static int roll(int bound){ // COUNTING FROM ONE
        return rand.nextInt(bound) + 1;
    }

    private static String boneses(String out, int prof, int bonus){
        if (prof != 0){
            out += " (+" + prof + ")";
        }

        if (bonus < 0) {
            out += " (" + bonus + ")";
        } else if (bonus > 0) {
            out += " (+" + bonus + ")";
        }

        return out;
    }
}
