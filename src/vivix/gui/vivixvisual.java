package vivix.gui;

import vivix.Data;
import vivix.Vivix;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class vivixvisual {

    private JButton tailButton;
    private JButton biteButton;
    private JButton clawButton;
    private JButton MultiTailButton;
    private JButton MultiClawButton;
    private JButton lightningBeamButton;
    private JButton clearButton;
    private JPanel Main;
    private JLabel IndiAttackLabel;
    private JLabel BreathAttackLabel;
    private JLabel MultiAttackLabel;
    private JPanel IndiAttackPanel;
    private JPanel BreathAttackPanel;
    private JPanel MultiAttackPanel;
    private JScrollPane OutputFieldScrollPane;
    private JTextArea OutputField;
    private JButton addButton;
    private JPanel parentPanel;
    private JPanel diceFrame;
    private JPanel bonuspanel;
    private JPanel d6panel;
    private JPanel d8panel;
    private JPanel d12panel;
    private JPanel d20panel;
    private JPanel d100panel;
    private JPanel dxpanel;
    private JButton d6Button;
    private JButton d8Button;
    private JButton d12Button;
    private JButton d20Button;
    private JButton d100Button;
    private JButton dxButton;
    private JSlider profbonusslider;
    private JPanel d4panel;
    private JButton d4Button;
    private JSlider otherbonusslider;
    private JLabel otherbonuslabel;
    private JLabel profbonuslabel;
    private JSlider amountOfDiceSlider;
    private JLabel amountOfDiceSliderValue;
    private JLabel amountOfDiceLabel;
    private JLabel profBonusSliderValue;
    private JLabel otherBonusSliderValue;
    private JButton d10Button;
    private JPanel d10panel;

    private int initProfBonus = 2;
    private int initOtherBonus = 0;

    public vivixvisual() {

        //profbonusslider.setValue(initProfBonus);
        //profBonusSliderValue.setText(String.valueOf(initProfBonus));
        //otherbonusslider.setValue(initOtherBonus);
        //otherBonusSliderValue.setText(String.valueOf(initOtherBonus));

        showSliderValue(profbonusslider, profBonusSliderValue);
        showSliderValue(otherbonusslider, otherBonusSliderValue);

        tailButton.addActionListener(e -> {
            addTextToOutput(Vivix.doAttack(Data.Tail));
        });

        biteButton.addActionListener(e -> {
            addTextToOutput(Vivix.doAttack(Data.Bite));
        });

        clawButton.addActionListener(e -> {
            addTextToOutput(Vivix.doAttack(Data.Claw));
        });

        MultiTailButton.addActionListener(e -> {
            addTextToOutput(Vivix.doMultiAttack(true));
        });

        MultiClawButton.addActionListener(e -> {
            addTextToOutput(Vivix.doMultiAttack(false));
        });

        lightningBeamButton.addActionListener(e -> {
            addTextToOutput(Vivix.doBreathAttack());
        });

        clearButton.addActionListener(e -> {
            clearOutput();
        });

        profbonusslider.addChangeListener(e -> {
            showSliderValue(profbonusslider, profBonusSliderValue);
        });

        otherbonusslider.addChangeListener(e -> {
            showSliderValue(otherbonusslider, otherBonusSliderValue);
        });

        d4Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(4));
        });

        d6Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(6));
        });

        d8Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(8));
        });

        d10Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(10));
        });

        d12Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(12));
        });

        d20Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(20));
        });

        d100Button.addActionListener(e -> {
            addTextToOutput(rollMoreD(100));
        });

        dxButton.addActionListener(e -> {
            addTextToOutput(rollMoreD(0));
        });
    }

    public void addTextToOutput(String add){
        OutputField.setText(OutputField.getText() + "\n" + line() + add);
    }

    public void clearOutput(){
        OutputField.setText("");
    }

    private static String line() {
        return "- - - - - - - - - - - - - - - - - - - - - - - - - - - -\n";
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Vivix Attacks");
        frame.setContentPane(new vivixvisual().Main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void showSliderValue(JSlider slider, JLabel label){
        int val = slider.getValue();
        if (val > 0) {
            label.setText("+" + val);
        } else {
            label.setText("" + val);
        }

    }

    private String rollMoreD(int d){
        return Dice.rollMore(amountOfDiceSlider.getValue(), d, profbonusslider.getValue(), otherbonusslider.getValue());
    }

}
