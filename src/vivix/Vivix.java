package vivix;

import vivix.gui.Dice;

import java.util.Random;

public class Vivix {

    public static String doMultiAttack(boolean withTail){
        String re = "";
        if (withTail){
            re += "MultiAttack: Bite, Claw, Tail: \n";
            re += doAttack(Data.Bite);
            re += doAttack(Data.Claw);
            re += doAttack(Data.Tail);
        } else {
            re += "MultiAttack: Bite, Claw, Claw: \n";
            re += doAttack(Data.Bite);
            re += doAttack(Data.Claw);
            re += doAttack(Data.Claw);
        }
        return re;
    }

    public static String doAttack(DragonAttackType attack){
        String returnString = "";

        int clawhit = Dice.roll(20);
        boolean clawcrit = (clawhit == 20);

        returnString += enter();

        returnString += "Hit > " + attack.type + ": " + clawhit + "(+ " + attack.addHit + " to hit) = " + (clawhit + attack.addHit) + "\n";

        int damageDie1 = Dice.roll(attack.dice);
        int damageDie2 = Dice.roll(attack.dice);

        int total = damageDie1 + damageDie2 + attack.addDamage;

        String output = "Damage > " + attack.type + ": " + damageDie1 + " + " + damageDie2 + " + " + attack.addDamage + " = " + total;

        if (clawcrit) {
            returnString += output + " x2 = " + (total * 2);
        } else {
            returnString += output;
        }

        //returnString += enter();

        return returnString + "\n";
    }

    public static String doBreathAttack(){
        String returnString = "Breath Attack: Lightning Beam: \n";

        int[] damageThrows = new int[10];
        int total = 0;

        for (int i = 0; i < damageThrows.length; i++) {
            int t = Dice.roll(10);
            damageThrows[i] = t;
            total += t;
        }

        returnString += "\nDamage > Beam: ";

        for (int i = 0; i < damageThrows.length; i++) {
            if (i == 0){
                returnString += " " + damageThrows[i] + " ";
            } else {
                returnString += " + " + damageThrows[i] + " ";
            }
        }

        returnString += " = " + total + "\n";

        returnString += "Total damage = " + total + "\n";

        return returnString;
    }

    private static String enter(){
        return "\n";
    }

}
