package vivix;

import vivix.gui.vivixvisual;

import java.util.Scanner;

public class Main {

    private Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        if (args.length > 0) {
            if (args[0].equals("-cm")){
                new Main().runCommandLine();
            }
        } else {
            //new Main().runWindow();
            vivixvisual.main(new String[]{});
        }

    }

    private void runWindow() {
        TopLevelWindow.createWindow();
    }

    public void runCommandLine(){
        System.out.println("\nvivix.Vivix MultiAttack Calculator: ");

        do {
            System.out.println("");
            Vivix.doMultiAttack(askYN("With Tail?"));
        } while (askYN("Again?"));

        System.out.println("Ok Bye");
    }

    private boolean askYN(String s){
        System.out.print("" + s + " (y/n): ");
        String in = sc.nextLine();

        if (in.equals("y") || in.equals("Y")){
            return true;
        } else {
            return false;
        }
    }
}
